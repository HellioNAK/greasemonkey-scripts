// ==UserScript==
// @name        getAllFiles
// @namespace   sirdonnie
// @description get urls of all files with metadata 
// @include     http://*
// @include     https://*
// @version     1
// @grant       none
// ==/UserScript==
var sirdonnie = unsafeWindow.sirdonnie = {};
sirdonnie.inExtensions = function(ext) {
  var extensions = [
    'pdf',
    'doc',
    'xls',
    'ppt'
  ];
  for (var i = 0; i<extensions.length; i++) 
    if (ext.indexOf(extensions[i]) > - 1) 
      return true;
  return false;
}
sirdonnie.getAllFiles = function() {
  var linkElements = document.getElementsByTagName('a');
  var imgElements = document.getElementsByTagName('img');
  var themUrls = {urls : []};
  var attToElement = {
    'href': linkElements,
    'src': imgElements
  };
  for (var attrib in attToElement) {
    var elems = attToElement[attrib];
    len = elems.length;
    
    if (len == 0)
      continue;
    for (var i=0; i<len;i++){
      var url = elems[i][attrib];
      var ext = url.split('.').pop().toLowerCase();

      if (window.sirdonnie.inExtensions(ext)) 
        themUrls["urls"].push(url);
    }
  }
  return themUrls;
}
